alias vimrc="vim ~/.vimrc"
alias tmuxrc="vim ~/.tmux.conf"
alias profile="vim ~/.profile"
alias readme="vim ~/pref/README.md"
alias g="git"
alias gg="git clone"
alias ga="git add -A :/"
alias gc="git commit -a -m"
alias gb="git checkout -B"
alias gx="git checkout"
alias gg="git pull"
alias gp="git push"
alias gs="git status"

export GOPATH="$HOME/go"
export PATH="$PATH:$GOPATH/bin"

# if running bash
if [ -n "$BASH_VERSION" ]; then
    # include .bashrc if it exists
    if [ -f "$HOME/.bashrc" ]; then
	. "$HOME/.bashrc"
    fi
fi

# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/bin" ] ; then
    PATH="$HOME/bin:$PATH"
fi
